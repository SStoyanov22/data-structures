﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReversedList
{
    //Implement a data structure ReversedList<T> that holds a sequence of elements of generic type T.
    //It should hold a sequence of items in reversed order.The structure should have some capacity that grows twice when it is filled.
    //The reversed list should support the following operations:

    //•	Add(T item)  adds an element to the sequence(grow twice the underlying array to extend its capacity in case the capacity is full)

    //•	Count  returns the number of elements in the structure

    //•	Capacity  returns the capacity of the underlying array holding the elements of the structure

    //•	this[index]  the indexer should access the elements by index (in range 0 … Count-1) in the reverse order of adding

    //•	Remove(index)  removes an element by index (in range 0 … Count-1) in the reverse order of adding

    //•	IEnumerable<T>  implement an enumerator to allow iterating over the elements in a foreach loop in a reversed order of their addition

    public class ReversedList<T> : IEnumerable<T>
    {

        private T[] arr;
        public ReversedList(int capacity)
        {
            arr = new T[capacity];
        }
        public int Count { get; private set; }

        public int Capacity { get; private set; }

        public void Add(T item)
        {
            var newArr = new T[arr.Length * 2];
            Array.Copy(arr, newArr, arr.Length);
            newArr[newArr.Length - 1] = item;
            this.arr = newArr;
        }

        public T Remove(int index)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            var index = 0;

            while(index < this.Count)
            {
                yield return arr[index];
                index++;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            this.GetEnumerator();
        }

        public T this[int index]
        {
            get
            {
                return arr[index];
            }
        }
    }
}
