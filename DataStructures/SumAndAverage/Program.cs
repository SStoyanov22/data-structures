﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumAndAverage
{

    //Write a program that reads from the console a sequence of integer numbers (on a single line, separated by a space).
    //Calculate and print the sum and average of the elements of the sequence. Keep the sequence in List<int>.
    class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split(' ');

            CalculateSumAndAverage(input);
            
        }

        private static void CalculateSumAndAverage(string[] input)
        {
            int n;
            bool isNumeric = int.TryParse(input[0], out n);

            if (isNumeric)
            {
                var numbers = input
                .Select(int.Parse).ToList();

                var sum = 0;

                foreach (var num in numbers)
                {
                    sum += num;
                }

                decimal avg = (decimal)sum / (decimal)numbers.Count;
                var result = String.Format("Sum = {0}; Average = {1}", sum, avg);
                Console.WriteLine(result);
            }
            else
            {
                Console.WriteLine();
            }
        } 
    }
}
