﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ArrayStack.Tests
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void PushPopElement()
        {
            var arr = new ArrayStack<int>();
            Assert.AreEqual(0, arr.Count);

            arr.Push(1);
            Assert.AreEqual(1, arr.Count);

            var popped = arr.Pop();
            Assert.AreEqual(1, popped);
            Assert.AreEqual(0, arr.Count);

        }
        [TestMethod]
        public void PushPopThousandElements()
        {
            var arr = new ArrayStack<int>();
            Assert.AreEqual(0, arr.Count);
            for (int i = 0; i < 1000; i++)
            {
                arr.Push(i);
                Assert.AreEqual(i + 1, arr.Count);
            }
            for (int i = 0; i < 1000; i++)
            {
                var num = arr.Pop();
                Assert.AreEqual(1000 - (i + 1), arr.Count);
                Assert.AreEqual(1000 - (i + 1), num);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void PopEmptyStack()
        {
            var arr = new ArrayStack<int>();

            arr.Pop();
        }

        [TestMethod]
        public void PushPopWithInitialCapacityOne()
        {
            var arr = new ArrayStack<int>();

            Assert.AreEqual(0, arr.Count);
            arr.Push(1);
            Assert.AreEqual(1, arr.Count);
            arr.Push(2);
            Assert.AreEqual(2, arr.Count);
            var num = arr.Pop();
            Assert.AreEqual(2, num);
            Assert.AreEqual(1, arr.Count);
            num = arr.Pop();
            Assert.AreEqual(1, num);
            Assert.AreEqual(0, arr.Count);
        }

        [TestMethod]
        public void ToArrayReversedArray()
        {
            var arr = new ArrayStack<int>();
            arr.Push(3);
            arr.Push(5);
            arr.Push(-2);
            arr.Push(7);
            var newArr = arr.ToArray();
            var compareArr = new int[] { 7, -2, 5, 3 };
            CollectionAssert.AreEqual(compareArr, newArr);
        }
    }

    
}
