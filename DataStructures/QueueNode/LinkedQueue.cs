﻿using System;

namespace QueueNode
{
    public class LinkedQueue<T>
    {
        public int Count { get; private set; }
        private QueueNode<T> head;
        private QueueNode<T> tail;
        public void Enqueue(T element)
        {
            var newNode = new QueueNode<T>(element);
            if (this.Count==0)
            {
                this.tail = this.head = newNode;
            }
            else
            {
                newNode.PrevNode = this.tail;
                this.tail.NextNode = newNode;
                this.tail = newNode;
            }

        }

        public T Dequeue()
        {
            if (this.Count == 0)
            {
                throw new InvalidOperationException();
            }
            var result = this.head.Value;
            var newHead = this.head.NextNode;
            newHead.PrevNode = null;
            this.head.NextNode = null;
            this.head = newHead;

            return result;
        }

        public T[] ToArray()
        {
            var arr = new T[this.Count];
            var current = this.head;
            var index = 0;
            while (current.NextNode != null)
            {
                arr[index] = current.Value;
                current = current.NextNode;
            }

            return arr;
        }


        private class QueueNode<T>
        {
            public QueueNode(T element)
            {
                this.Value = element;
            }
            public T Value { get; private set; }
            public QueueNode<T> NextNode { get; set; }
            public QueueNode<T> PrevNode { get; set; }
        }    
    }
}
