﻿using System;

namespace ArrayStack
{
    public class ArrayStack<T>
    {
        private T[] elements;
        public int Count { get; private set; }

        private const int InitialCapacity = 16;

        public ArrayStack ( int capacity = InitialCapacity)
        {
            this.elements = new T[capacity];
        }

        public void Push(T element)
        {
            //If ArrayStack is full grow the array
            if (this.Count >= this.elements.Length)
            {
                Grow();
            }

            this.elements[this.Count] = element;
            this.Count++;
        }

        public T Pop()
        {
            if (this.Count ==0)
            {
                throw new InvalidOperationException();
            }
            var result = this.elements[this.Count-1];
            this.elements[this.Count] = default;
            this.Count--;

            return result;
        }

        public T[] ToArray()
        {
            
            var newArr = new T[this.Count];
            var count = 0;
            while (this.Count>0)
            {
                newArr[count] = this.Pop();
                count++;
            }
            return newArr;
        }

        public void Grow()
        {
            var newArr = new T[this.elements.Length * 2];
            Array.Copy(this.elements, newArr, this.Count);
            this.elements = newArr; 
        }

    }


    class Example
    {
        static void Main()
        {
            var list = new ArrayStack<int>();
            Console.WriteLine("--------------------");

            list.Push(5);
            list.Push(3);
            list.Push(2);
            list.Push(10);
            Console.WriteLine("Count = {0}", list.Count);
            Console.WriteLine("--------------------");

            Console.WriteLine(list.Pop());
            Console.WriteLine(list.Pop());
            Console.WriteLine(list.Pop());
            Console.WriteLine("--------------------");

            Console.WriteLine(list.Pop());
            Console.WriteLine("--------------------");
        }
    }


}
