﻿using Node;
using System;

namespace LinkedStack
{
    public class LinkedStack<T>
    {
        private Node<T> firstNode;
        public int Count { get; private set; }
        public void Push(T element)
        {
            var newNode = new Node<T>(element);

            if (this.Count == 0)
            {
                this.firstNode = newNode.Next;
            }
            else
            {
                newNode.Next = this.firstNode;
                this.firstNode = newNode.Next;
            }

            
        }
        public T Pop()
        {
            var result = this.firstNode.Value;
            var nextNode = this.firstNode.Next;
            this.firstNode.Next = null;
            this.firstNode = nextNode;

            return result;
        }

        public T[] ToArray()
        {
            var arr = new T[this.Count];
            var current = this.firstNode;
            var index = 0;
            while (current.Next != null)
            {
                arr[index] = current.Value;
                current = current.Next;
            }

            return arr;
            
        }
    }
}
