﻿using System;

namespace StupidList
{
    //A simple implementation of a list
    public class StupidList<T>
    {
        private T[] arr = new T[0];

        public int Length 
        { 
            get
            {
                return this.arr.Length;
            } 
        }

        public T this[int index] 
        { 
            get
            {
                return this.arr[index];
            }        
        }

        public T First 
        { 
            get
            {
                return this.arr[0];
            }
        }

        public T Last
        {
            get
            {
                return this.arr[arr.Length-1];
            }
        }

        public void Add (T item)
        {
            //Create new array with larger size
            var newArr = new T[this.arr.Length + 1];

            //Copy old array to new array
            Array.Copy(this.arr, newArr, this.arr.Length);

            //add new element to the end of the new array
            newArr[newArr.Length - 1] = item;

            //change refference to new array
            this.arr = newArr;
        }

        public T Remove(int index)
        {
            //get element at index
            T result = this.arr[index];

            //create new array with smaller size
            var newArr = new T[this.arr.Length - 1];
            
            //copy elements until the index
            Array.Copy(this.arr, newArr, index);

            //copy elements after the index
            Array.Copy(this.arr, index+1, newArr, index + 1, this.arr.Length - index - 1);

            //change refference to new array
            this.arr = newArr;

            return result;
        }

        public T RemoveFirst()
        {
            return this.Remove(0);
        }

        public T RemoveLast()
        {
            return this.Remove(this.Length - 1);
        }



    }
}
