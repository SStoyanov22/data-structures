﻿namespace TriverseAndSaveFoldersUsingTree
{
    public class File
    {
        private int size;
        private string name;

        public File(int size, string name)
        {
            this.size = size;
            this.name = name;
        }
        public int Size { get; set; }
        public string Name { get; set; }
    }
}
