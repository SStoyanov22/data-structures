﻿using System.Collections.Generic;

namespace TriverseAndSaveFoldersUsingTree
{
    public class Folder
    {
        private string name;
        private List<File> files;
        public Folder(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }

        public IList<File> Files { get; set; }
    }
}
