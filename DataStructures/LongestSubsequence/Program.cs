﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongestSubsequence
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = Console.ReadLine().Split(' ').Select(int.Parse).ToList();

            LongestSubsequence(numbers);

            
        }

        private static void LongestSubsequence(List<int> numbers)
        {
            var count = 0;
            var maxCount = 1;
            int maxSubsequentNumber = numbers[0];

            var checkedNumbers = new List<int>();
            for (int i = 0; i < numbers.Count; i++)
            {
                var currentNumber = numbers[i];
                if (checkedNumbers.Contains(currentNumber))
                {
                    continue;
                }
                else
                {
                    checkedNumbers.Add(currentNumber);
                    count++;
                    for (int j = i + 1; j < numbers.Count; j++)
                    {
                        if (currentNumber != numbers[j])
                        {
                            count = 0;
                            break;
                        }
                        else
                        {
                            count++;
                            if (count > maxCount)
                            {
                                maxCount = count;
                                maxSubsequentNumber = currentNumber;
                            }

                        }
                    }
                }
            }

            for (int i = 0; i < maxCount; i++)
            {
                Console.Write(maxSubsequentNumber.ToString() + " ");
            }
            Console.WriteLine();
        }
    }
}
