﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundDance
{
    class Program
    {
        static int maxlength = 0;
        static void Main(string[] args)
        {
            int friendships = int.Parse(Console.ReadLine());
            int lead = int.Parse(Console.ReadLine());

            var partners = new Dictionary<int, List<int>>();
            var visited = new List<int>();

            for (int i = 0; i < friendships; i++)
            {
                var friends = Console.ReadLine().Split(new char[] { ' ' }).Select(int.Parse).ToArray();

                if (!partners.ContainsKey(friends[0]))
                {
                    partners[friends[0]] = new List<int>() { friends[1] };
                }
                else
                {
                    partners[friends[0]].Add(friends[1]);
                }



                if (!partners.ContainsKey(friends[1]))
                {
                    partners[friends[1]] = new List<int>() { friends[0] };
                }
                else
                {
                    partners[friends[1]].Add(friends[0]);
                }
            }
            FindLongestRoundDance(partners, visited, lead, 0);

            Console.WriteLine(maxlength);
        }

        private static void FindLongestRoundDance(Dictionary<int, List<int>> partners, List<int> visited, int lead, int length)
        {
            visited.Add(lead);
            length += 1;
            if (length>maxlength)
            {
                maxlength = length;
            }
            foreach (var child in partners[lead])
            {
                if (!visited.Contains(child))
                {
                    FindLongestRoundDance(partners, visited, child, length);
                }
            }
            
        }
    }
}
