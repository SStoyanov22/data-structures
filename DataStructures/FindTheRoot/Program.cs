﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tree;

namespace FindTheRoot
{
//    Input:
//•	Read the input data from the console.
//•	The first line holds the number of nodes N.
//•	The first line holds the number of edges M.
//•	Each of the following M lines hold the edges: two numbers separated by a space.The first number is the parent node; the second number is the child node.

//Output:
//•	Print at the console the number of the root node in case the graph is a tree.
//•	If there is no root, print “No root!” at the console.
//•	If multiple root nodes exist, print "Multiple root nodes!" at the console.

    class Program
    {
        static void Main(string[] args)
        {
            //nodes
            var n = int.Parse(Console.ReadLine());
            //edges
            var m = int.Parse(Console.ReadLine());
            var hasParent = new bool[n];
            var line = Console.ReadLine();
            for (int edge = 0; edge < m; edge++)
            {
                var nums = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
                var childValue = nums[1];
                hasParent[childValue] = true;
                line = Console.ReadLine();
            }

            var countNodesWithoutParents = 0;
            int root=0;
            for (   int nodeValue = 0; nodeValue < hasParent.Length; nodeValue++)
            {
                if (!hasParent[nodeValue])
                {
                    countNodesWithoutParents++;
                    root = nodeValue;
                }
            }

            if (countNodesWithoutParents == 0)
            {
                Console.WriteLine("No root!");
            }
            else if (countNodesWithoutParents == 1)
            {
                Console.WriteLine(root.ToString());
            }
            else
            {
                Console.WriteLine("Multiple root nodes!");
            }
        }
    }
}
