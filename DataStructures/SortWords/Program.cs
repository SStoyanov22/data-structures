﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortWords
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split(' ').ToList<string>();

            SortWords(input);

            Console.WriteLine(string.Join(" ", input.ToArray()));
        }

        private static void SortWords(List<string> words)
        {
            for (int i = 0; i < words.Count; i++)
            {
                for (int j = i+1; j < words.Count; j++)
                {
                    if (string.Compare(words[i],words[j]) > 0 )
                    {
                        var tmp = words[i];
                        words[i] = words[j];
                        words[j] = tmp;
                    }
                }
            }
        }
    }
}
