﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideTheHorse
{
    class Program
    {
        static int[,] field;
        static bool[,] visited;
        static void Main(string[] args)
        {
            var n = int.Parse(Console.ReadLine());
            var m = int.Parse(Console.ReadLine());
            var r = int.Parse(Console.ReadLine());
            var c = int.Parse(Console.ReadLine());

            field = new int[n, m];
            visited = new bool[n, m];
            field[r, c] = 1;
            visited[r, c] = true;
            
            
            field[r, c] = 1;
            var queue = new Queue<Point>();
            queue.Enqueue(new Point(r, c,1));
            BFS(queue);

            PrintField();
        }

        private static void PrintField()
        {
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    Console.Write(field[i,j]);
                }
                Console.WriteLine();
            }
        }

        private static void BFS(Queue<Point> queue)
        {
            while (queue.Count > 0)
            {
                var currentPoint = queue.Dequeue();
   
                IList<Point> moveToPositions = new List<Point>();
                FindPositionsToMove(currentPoint, moveToPositions);
                foreach (var item in moveToPositions)
                {
                    if (!visited[item.Row, item.Column])
                    {
                        queue.Enqueue(item);
                        visited[item.Row, item.Column] = true;
                    }
                }
            }

        }

        private static IList<Point> FindPositionsToMove(Point currentPoint, IList<Point> moveToPositions)
        {
            var x = currentPoint.Row;
            var y = currentPoint.Column;
            var counter = currentPoint.Counter + 1;
            if (x - 2 >=0 && y -1 >=0 && !visited[x - 2,y - 1])
            {
                moveToPositions.Add(new Point(x - 2, y - 1, counter));
                field[x - 2, y - 1] = counter;
            }

            if (x - 2 >= 0 && y + 1 < field.GetLength(1) && !visited[x - 2, y + 1])
            {
                moveToPositions.Add(new Point(x - 2, y + 1, counter));
                field[x - 2, y + 1] = counter;
            }

            if (x + 2 < field.GetLength(0) && y - 1 >= 0 && !visited[x + 2, y - 1])
            {
                moveToPositions.Add(new Point(x + 2, y - 1, counter));
                field[x + 2, y - 1] = counter;
            }

            if (x + 2 < field.GetLength(0) && y + 1 < field.GetLength(1) && !visited[x + 2, y + 1])
            {
                moveToPositions.Add(new Point(x + 2, y + 1, counter));
                field[x + 2, y + 1] = counter;
            }


            if (x - 1 >= 0 && y - 2 >= 0 && !visited[x - 1, y - 2])
            {
                moveToPositions.Add(new Point(x - 1, y - 2, counter));
                field[x - 1, y - 2] = counter;
            }

            if (x - 1 >= 0 && y + 2 < field.GetLength(1) && !visited[x - 1, y + 2])
            {
                moveToPositions.Add(new Point(x - 1, y + 2, counter));
                field[x -1, y + 2] = counter;
            }

            if (x + 1 < field.GetLength(0) && y - 2 >= 0 && !visited[x + 1, y - 2])
            {
                moveToPositions.Add(new Point(x + 1, y - 2, counter));
                field[x + 1, y - 2] = counter;
            }

            if (x + 1 < field.GetLength(0) && y + 2 < field.GetLength(1) && !visited[x + 1, y + 2])
            {
                moveToPositions.Add(new Point(x + 1, y + 2,counter));
                field[x + 1, y + 2] = counter;
            }

            return moveToPositions;
        }
    }

    public class Point
    {

        public Point(int row, int col, int counter)
        {
            this.Row = row;
            this.Column = col;
            this.Counter = counter;
        }
        public int Row { get; set; }

        public int Column { get; set; }

        public int Counter { get; set; }
    }
}
