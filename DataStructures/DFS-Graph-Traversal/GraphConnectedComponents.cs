﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Trees 
{ 
    public class GraphConnectedComponents
    {
        static bool[] visited;
        static  List<int>[] graph;
        public static void Main()
        {
            ReadGraph();
            FindGraphVisitedComponents();
            
        }

        private static void FindGraphVisitedComponents()
        {
            visited = new bool[graph.Length];

            for (int startNode = 0; startNode < visited.Length; startNode++)
            {
                if (!visited[startNode])
                {
                    Console.Write("Connected component:");
                    DFS(startNode);
                    Console.WriteLine();
                }
            }
        }

        static new List<int>[] ReadGraph()
        {
            int n = int.Parse(Console.ReadLine());
            visited = new bool[n];
            graph = new List<int>[n];
            for (int i = 0; i < n; i++)
            {
                var input = Console.ReadLine();
                graph[i] = input
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToList();
            }

            return graph;
        }
        static void DFS(int node)
        {
            if (!visited[node])
            {
                visited[node] = true;
                foreach (var child in graph[node])
                {
                    DFS(child);
                }
                Console.Write(" " + node);
            }
        }
    }
}