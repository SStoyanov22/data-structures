﻿using System;

namespace Node
{
    public class Node<T>
    {
        public Node<T> Next{ get; set; }

        public T Value { get; private set; }
        public Node(T value, Node<T> next = null)
        {
            this.Value = value;
            this.Next = next;
        }
    }
}
